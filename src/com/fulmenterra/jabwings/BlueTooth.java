package com.fulmenterra.jabwings;

import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.util.Log;
import android.widget.Toast;

public class BlueTooth{
	
	private Resources res;
	private OutputStream salida;
	private BluetoothAdapter adaptadorBT;
	private BluetoothDevice cuadricoptero;
	private BluetoothSocket socket;
	final private Espera outer;
	
	public BlueTooth(Espera espera){
		this.outer = espera;
		this.res = espera.getResources();
	}
	
	/**
	 * Inicia busqueda del dispositivo si se encuentra se pasa a conectar()
	 * si no, se cambia el estado a error de conexion, el proceso de busqueda
	 * dura 12 segundos o menos si se encuentra el cuadricoptero.
	 */
	public void buscar(){
		outer.cambiarEstado(1);
		//Activa un BroadcastReceiver para ver los dispotivos que va encontran
		adaptadorBT.startDiscovery();
	}
	private void conectar(final BluetoothDevice cuadricoptero){
		outer.cambiarEstado(2);
		
		this.cuadricoptero = cuadricoptero;
		
		new Thread(){
			public void run(){
				
				try {
					UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"); //Codigo de servicio
					socket = cuadricoptero.createRfcommSocketToServiceRecord(uuid);
					socket.connect(); //Se conecta al dispositivo
					outer.runOnUiThread(new Runnable(){
						public void run() {
							outer.cambiarEstado(4);
						}
					});
					//Stream de salida
					salida = socket.getOutputStream();
				} catch (IOException e) {
					outer.runOnUiThread(new Runnable(){
						public void run() {
							outer.cambiarEstado(3);
						}
					});
					try {
		                socket.close();
		            } catch (IOException closeException) { closeException.printStackTrace(); }
				} 
			}
		}.start();
	}
	public void desconectar(){
		if (socket != null)
			try {
				enviar("ot");
				salida.close();
				socket.close();
			} catch (IOException e){
				try {
					salida.close();
					socket.close();
				} catch (IOException e1) {}
			}
	}
	public void enviar(String texto){
		try {
			salida.write(texto.getBytes());
			for (int largo = texto.length(); largo < 4; largo++)
				salida.write(0x00);
		} catch (IOException e) {}
	}
	/**
	 * Revisa si existe adaptador, luego ve si esta activado, si no lo est� se le pregunta
	 * al usuario si desea activarlo 
	 * @return <ul>
	 * <li> true, si esta activado o el usuario lo activo mediante el dialogo</li>
	 * <li> false, si no existe o el usuario no quizo activarlo en el dialogo</li>
	 * </ul>
	 */
	private boolean activarAdaptador(){
		this.adaptadorBT = BluetoothAdapter.getDefaultAdapter();
		//Revisamos si existe alg�n adaptador BT
		if (adaptadorBT == null) { //Si no existe avisa y termina
			terminarApp();
			return false;
		}
		else {
			//Chequeamos si esta activado si no lo esta pedimos activarlo mediante un dialogo
			if (!adaptadorBT.isEnabled()) {
			    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			    outer.startActivityForResult(enableBtIntent, 1); 
			    int estadoAdaptador = adaptadorBT.getState();
				if ((estadoAdaptador != BluetoothAdapter.STATE_ON) || 
						(estadoAdaptador != BluetoothAdapter.STATE_TURNING_ON)) 
					return false; 
			}
		}
		return true;
	}
	/**
	 * Muestra un dialogo avisando la necesidad de Bluetooth, 
	 * termina la aplicacion
	 */
	private void terminarApp() {
	    AlertDialog.Builder builder = new AlertDialog.Builder(outer);
	    
	    builder.setTitle(res.getString(R.string.sinadaptador_dialog_title));
	    builder.setMessage(res.getString(R.string.sinadaptador_dialog_message));
	    builder.setPositiveButton("Terminar", new OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) {
	            dialog.cancel();
	            outer.finish();
	        }
	    });
	    builder.setOnCancelListener(new DialogInterface.OnCancelListener(){
	        public void onCancel(DialogInterface dialog){
	             outer.finish();
	        }
	    });
	    builder.create().show();
	}

	public void onStart(){
		//Filtro para ver si en algun momento se apaga el bt.
		IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
		outer.registerReceiver(accionBT, filter);
		//Revisamos si un dispositivo ha sido encontrado
		filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		outer.registerReceiver(accionBT, filter); 
		//Cuando terminemos de descubrir dispositivos
		filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		outer.registerReceiver(accionBT, filter);
		//En caso de que el dispositivo se desconecte
		filter = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED);
		outer.registerReceiver(accionBT, filter);
		
		if (activarAdaptador()){
			buscar();
		}
	}
	public void onStop(){
		outer.unregisterReceiver(accionBT);
		outer.desconectar();
	}
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		if (requestCode == 1){
			if (resultCode == Activity.RESULT_CANCELED) terminarApp(); //Si se cancela o termina el dialogo, termina la app
		}
	}
	private BroadcastReceiver accionBT = new BroadcastReceiver() {
	    public void onReceive(Context context, Intent intent) {
	        String action = intent.getAction();
	        
	        //Cuando se encuentra un dispositivo
	        if (BluetoothDevice.ACTION_FOUND.equals(action)) {
	            //Obtener dispositivo desde el intent
	            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
	          //Si el dispositivo es el correcto se detiene el descubrimiento de nuevos
	            if (((device != null) && (device.getName() != null)) && 
	            		device.getName().equals("Jab Wings")){
	            	adaptadorBT.cancelDiscovery();
	            	conectar(device);
	            }
	        }
	        else if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)){
	        	int estado = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, 0);
	        	if (estado == BluetoothAdapter.STATE_TURNING_OFF){
	        		activarAdaptador();
	        	}
	        }
	        else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
	        	new Thread(){
	        		public void run(){
	        			try {Thread.sleep(3000);
	        			} catch (InterruptedException e) {}
	        			if (cuadricoptero == null){
	        				outer.runOnUiThread(new Runnable(){
		        				public void run(){
		        					outer.cambiarEstado(3);
		        				}
		        			});
	        			}
	        		}
	        	}.start();
	        }
	        else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)){
	        	BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
	        	if (device.getName().equals("Jab Wings")){
	        		Log.i("Jab Wings - Bluetooth", "Cuadricoptero desconectado");
	        		outer.cambiarEstado(0);
	        		Toast.makeText(outer.getApplicationContext(), 
	        				"Cuadricoptero Desconectado", Toast.LENGTH_SHORT).show();
	        	}
	        }
	    }
	};
}
