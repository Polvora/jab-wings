package com.fulmenterra.jabwings;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class Sensores implements SensorEventListener{
	
    final private Espera outer;
    final private Resources res;
    private SensorManager sensorManager;
    private Sensor acelerometro;
    private Sensor magnetico;
	final private float[] valuesMagnet      = new float[3];
    final private float[] valuesAccel       = new float[3];
    final private float[] valuesOrientation = new float[3];
    final private float[] rotationMatrix    = new float[9];
    
    public Sensores(Espera espera){
    	this.outer = espera;
    	this.res = espera.getResources();
    	sensorManager = (SensorManager) outer.getSystemService(Context.SENSOR_SERVICE);
    	acelerometro = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    	magnetico = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
    }
    public float[] obtenerDatos(){
    	SensorManager.getRotationMatrix(rotationMatrix, null, valuesAccel, valuesMagnet);
        SensorManager.getOrientation(rotationMatrix, valuesOrientation);
        for (int i = 0; i < 3; i++)
        	valuesOrientation[i] = (float) Math.toDegrees(valuesOrientation[i]);
    	return valuesOrientation;
    }
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}
	@Override
	public void onSensorChanged(SensorEvent event) {
		switch (event.sensor.getType()) {
        case Sensor.TYPE_ACCELEROMETER:
            System.arraycopy(event.values, 0, valuesAccel, 0, 3);
            break;

        case Sensor.TYPE_MAGNETIC_FIELD:
            System.arraycopy(event.values, 0, valuesMagnet, 0, 3);
            break;
        }
	}
	private void terminarApp() {
	    AlertDialog.Builder builder = new AlertDialog.Builder(outer);
	    
	    builder.setTitle(res.getString(R.string.sinsensor_dialog_title));
	    builder.setMessage(res.getString(R.string.sinsesor_dialog_message));
	    builder.setPositiveButton("Terminar", new OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) {
	            dialog.cancel();
	            outer.finish();
	        }
	    });
	    builder.setOnCancelListener(new DialogInterface.OnCancelListener(){
	        public void onCancel(DialogInterface dialog){
	             outer.finish();
	        }
	    });
	    builder.create().show();
	}
	public boolean onStart(){
        if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) == null ||
        		sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) == null){
        	terminarApp();
        	return false;
        }
        sensorManager.registerListener(this, acelerometro, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, magnetico, SensorManager.SENSOR_DELAY_NORMAL);
        return true;
	}
	public void onStop(){
		sensorManager.unregisterListener(this);
	}
}
