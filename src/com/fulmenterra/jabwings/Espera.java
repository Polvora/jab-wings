package com.fulmenterra.jabwings;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Espera extends Activity {
	
	private int estado = 0;
	private boolean enviarDatos;
	private Resources res;
	private ImageView imagen;
	private TextView estado1;
	private TextView estado2;
	private Button boton;
	private BlueTooth bt;
	private Sensores sensores;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.espera);
		
		//Obtenemos los recursos del sistema
		this.res = getResources();
		bt = new BlueTooth(this); //Obtenemos una instancia BlueTooth para trabajar con ella
		sensores = new Sensores(this);
		
		TextView logo = (TextView) findViewById(R.id.logo);
		this.estado1 = (TextView) findViewById(R.id.estado1);
		this.estado2 = (TextView) findViewById(R.id.estado2);
		this.boton = (Button) findViewById(R.id.boton);
		this.imagen = (ImageView) findViewById(R.id.imagen);
		
		cambiarEstado(0);		
		logo.setTypeface(Typeface.createFromAsset(getAssets(),"fonts/KoolBean.ttf")); //Seleccion de fuente personalizada
		
		boton.setOnClickListener(botonListener);
	}
	
	@Override
	protected void onStart(){
		super.onStart();
		bt.onStart();
		sensores.onStart();
	}
	@Override
	protected void onStop(){
		super.onStop();
		bt.onStop();
		sensores.onStop();
	}
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		bt.onActivityResult(requestCode, resultCode, data);
	}
	protected void cambiarEstado(int estado){
		this.estado = estado;
		switch (estado){
		case 0: //No conectado
			boton.setEnabled(true);
			boton.setText(res.getString(R.string.boton_conectar));
			estado1.setText(res.getString(R.string.estado1_noconectado));
			estado2.setText(res.getString(R.string.estado2_noconectado));
			imagen.setImageDrawable(res.getDrawable(R.drawable.telefonoondas2));
			break;
		case 1: //Escaneando 
			boton.setEnabled(false);
			boton.setText(res.getString(R.string.boton_reintentar));
			estado1.setText(res.getString(R.string.estado1_escaneando));
			estado2.setText(res.getString(R.string.estado2_escaneando));
			break;
		case 2: //Conectando
			boton.setEnabled(false);
			boton.setText(res.getString(R.string.boton_reintentar));
			estado1.setText(res.getString(R.string.estado1_conectando));
			estado2.setText(res.getString(R.string.estado2_conectando));
			break;
		case 3: //Conexion fallida
			boton.setEnabled(true);
			boton.setText(res.getString(R.string.boton_reintentar));
			estado1.setText(res.getString(R.string.estado1_noconectado));
			estado2.setText(res.getString(R.string.estado2_noconectado));
			break;
		case 4: //Conectado
			boton.setEnabled(true);
			boton.setText(res.getString(R.string.boton_enciende));
			estado1.setText(res.getString(R.string.estado1_conectado));
			estado2.setText(res.getString(R.string.estado2_conectado));
			imagen.setImageDrawable(res.getDrawable(R.drawable.cuadricoptero1));
			break;
		case 5: //Iniciado
			boton.setEnabled(true);
			boton.setText(res.getString(R.string.boton_desconetar));
			estado1.setText(res.getString(R.string.estado1_iniciado));
			estado2.setText(res.getString(R.string.estado2_iniciado));
			break;
		}
	}
	public void volar(){
		cambiarEstado(5);
		bt.enviar("oa");
		final float[] data = new float[3];
		this.enviarDatos = true;
		new Thread(){
			public void run(){
				while(enviarDatos){
					System.arraycopy(sensores.obtenerDatos(), 0, data, 0, 3);
					//bt.enviar("inclinacion" + ";" + data[0] + ";" + data[1] + ";" + data[2] + ";");
					//bt.enviar("b;b;");
					try {Thread.sleep(250);
					} catch (InterruptedException e) {}
				}
			}
		}.start();
	}
	public void desconectar(){
		cambiarEstado(0);
		this.enviarDatos = false;
		bt.desconectar();	
	}
	View.OnClickListener botonListener = new View.OnClickListener() {
		public void onClick(View v) {
			switch (estado){
			case 0: case 3: 
				bt.buscar();
				break;
			case 4:
				volar();
				break;
			case 5:
				desconectar();
				break;
			}
		}
	};
}
