package com.fulmenterra.jabwings;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;

public class Splash extends Activity {
	
	Resources res;
	private LinearLayout pantalla;
	private boolean screenPressed = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		
		TextView logo = (TextView) findViewById(R.id.logo);
		pantalla = (LinearLayout) findViewById(R.id.pantalla);
		this.res = getResources();  //Obtenemos los recursos
		//Seleccionamos la fuente personalizada
		logo.setTypeface(Typeface.createFromAsset(getAssets(),"fonts/KoolBean.ttf"));
		
		pantalla.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				screenPressed = true;
				startActivity(new Intent(Splash.this,Espera.class));
				finish();
				return false;
			}
		});
		new Thread(){
			public void run(){
				try{ Thread.sleep(3000); } 
				catch (Exception e){ finish(); }
				runOnUiThread(new Runnable(){
					public void run() {
						if (!screenPressed) {
							startActivity(new Intent(Splash.this,Espera.class));
							finish();
						}
					}
				});
			}
		}.start();
	}
}
